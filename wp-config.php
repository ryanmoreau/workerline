<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'worker_' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Rigl2Sx*chDvu%Z%PAG8$+`Z7WMA5hnSQD|4dH-BU!7=P@^%.${NW.Rff{:8lyS!' );
define( 'SECURE_AUTH_KEY',  '`KDB&9FFSYT&Tva/VR8~*,talp`qK/$kh!R[AHusnIGSAtwm;Q1244=K Yry@Jr,' );
define( 'LOGGED_IN_KEY',    'T82uitBMW|}o}:6jEl^_I|^*:bX`t{hR|x)k%<vhm|Jbat#VQ`,[V-fCvujEnFD@' );
define( 'NONCE_KEY',        'K`!,ij lI=]w@vWmMOZhKjzrKWA|mzZd;}dpZ2}GX:z1_2J^~&i4}(53=?HULF|8' );
define( 'AUTH_SALT',        ';!OJ,^gUz`nGh;j0N%HwGkfy+9PDY;2UrS:}fUQP(6K9,gl=Ajg%HN8R=@GG6/js' );
define( 'SECURE_AUTH_SALT', 'O#Y4e;x*pa^t%`i8Rz`oswSLF96KfNMkVo.O~*d,v1G^p )}^ZF|p+R<7<(HK8G?' );
define( 'LOGGED_IN_SALT',   'sIic^yq)_ba2akL#vTy_)WEzHYw<7 h]bE30gV,JC?4CbCVbs*VC@)D})@>ii4VI' );
define( 'NONCE_SALT',       ',qV!;Cl}a~LDw,Q.~N,rfK$vY@d,A(_jh+>jiw++{%z*gKNk>=@;a~qrMt@DWael' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wline_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
